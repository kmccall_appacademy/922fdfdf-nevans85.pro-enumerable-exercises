require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| substring?(string, substring) }
end

def substring?(string_to_check, substring)
  string_to_check.include?(substring)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.delete!(' ')
  chars = string.split('')
  chars.sort.select { |char| chars.count(char) > 1 }.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  words = string.split(' ')
  words.sort_by!(&:length)
  [words[-1], words[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  all_letters = 'abcdefghijklnopqrstuvwxyz'
  all_letters.delete(string).split('')
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  results = []
  (first_yr..last_yr).each { |year| results << year if not_repeat_year?(year) }
  results
end

def not_repeat_year?(year)
  str_digits = year.to_s.split('')
  str_digits == str_digits.uniq
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  results = []
  songs.uniq.each { |song| results << song if no_repeats?(song, songs) }
  results
end

def no_repeats?(song, songs)
  songs.each_with_index do |_el, idx|
    return false if song == songs[idx] && song == songs[idx + 1]
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split
  best = ''
  words.map! { |word| remove_punctuation(word) }
  words.each { |word| best = word if c_distance(word) < c_distance(best) }
  best
end

def c_distance(word)
  chars = word.split('')
  distance = 100
  chars.each_with_index do |char, idx|
    distance = word.length - idx - 1 if char.downcase == 'c'
  end
  distance
end

def remove_punctuation(word)
  letters = %w[q w e r t y u i o p a s d f g h j k l z x c v b n m]
  chars = word.downcase.split('')
  chars.select { |char| letters.include?(char) }.join
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  results = []
  arr.each_with_index do |_num, idx|
    if arr[idx] == arr[idx + 1] && arr[idx] != arr[idx - 1]
      results << [idx, idx + length_of_repeating_series(idx, arr) - 1]
    end
  end
  results
end

def length_of_repeating_series(idx, arr)
  length = 1
  length += 1 while arr[idx] == arr[idx + length]
  length
end
